package com.adit.product.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adit.product.R
import com.adit.product.databinding.ItemCategoryBinding
import com.adit.product.model.DataCategory
import com.bumptech.glide.Glide

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.ListViewHolder>() {
    private val listCategory = ArrayList<DataCategory>()

    fun setData(items: ArrayList<DataCategory>) {
        listCategory.clear()
        listCategory.addAll(items)
        notifyDataSetChanged()
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemCategoryBinding.bind(itemView)

        fun bind(category: DataCategory) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(category.icon)
                    .into(iconCategory)

                textCategoryName.text = category.nameCategory
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val mView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return ListViewHolder(mView)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listCategory[position])
    }

    override fun getItemCount(): Int = listCategory.size
}