package com.adit.product.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.adit.product.R
import com.adit.product.databinding.ItemGridProductBinding
import com.adit.product.model.DataProduct
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ListViewHolder>() {
    private val listProduct = ArrayList<DataProduct>()

    fun setData(items: ArrayList<DataProduct>){
        listProduct.clear()
        listProduct.addAll(items)
        notifyDataSetChanged()
    }
    class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding = ItemGridProductBinding.bind(itemView)

        fun bind(product: DataProduct){
            with(binding){
                Glide.with(itemView.context)
                    .load(product.photoProduct)
                    .apply(RequestOptions().override(350,550))
                    .into(imageProduct)

                textProductName.text = product.nameProduct
                textPriceProduct.text = itemView.resources.getString(R.string.price,product.price)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val mView = LayoutInflater.from(parent.context).inflate(R.layout.item_grid_product,parent,false)
        return ListViewHolder(mView)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
       holder.bind(listProduct[position])
    }

    override fun getItemCount(): Int = listProduct.size
}