package com.adit.product.mvvm

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.adit.product.activity.MainActivity
import com.adit.product.model.DataBanner
import com.adit.product.model.DataCategory
import com.adit.product.model.DataProduct
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class MainViewModel : ViewModel() {

    private val productList = MutableLiveData<ArrayList<DataProduct>>()
    private val categoryList = MutableLiveData<ArrayList<DataCategory>>()
    private val bannerList = MutableLiveData<List<DataBanner>>()

    fun setDataProduct() {
        val list = ArrayList<DataProduct>()

        val client = AsyncHttpClient()
        val url = "https://gardien.tokodistributor.co.id/api-web/v2/product-recommendation?page=1"

        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
                val result = String(responseBody!!)
                Log.d("API Result", result)

                try {
                    val responseObjects = JSONObject(result)
                    val data = responseObjects.getJSONArray("data")

                    for (i in 0 until data.length()) {
                        val jsonObject = data.getJSONObject(i)

                        val dataProductList = DataProduct()
                        dataProductList.photoProduct = jsonObject.getString("image_uri")
                        dataProductList.nameProduct = jsonObject.getString("product_name")
                        dataProductList.price = jsonObject.getString("normal_price")
                        list.add(dataProductList)
                    }
                    productList.postValue(list)

                } catch (e: Exception) {
                    Log.d("API Result Error", "Error: ${e.message}")
                    Toast.makeText(MainActivity(), e.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable?
            ) {
                when (statusCode) {
                    401 -> "$statusCode : Bad Request"
                    403 -> "$statusCode : Forbidden"
                    404 -> "$statusCode : Not Found"
                    else -> "$statusCode : ${error?.message}"
                }

                Log.d("OnFailure", statusCode.toString())
            }

        })
    }

    fun getDataProduct(): LiveData<ArrayList<DataProduct>> {
        return productList
    }

    fun setDataCategory() {
        val list = ArrayList<DataCategory>()

        val client = AsyncHttpClient()
        val url = "https://gardien.tokodistributor.co.id/api-web/v2/utility/home/box-category?with_staple=true"

        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
                val result = String(responseBody!!)
                Log.d("API Result", result)

                try {
                    val responseObjects = JSONObject(result)
                    val data = responseObjects.getJSONArray("data")

                    for (i in 0 until data.length()) {
                        val jsonObject = data.getJSONObject(i)

                        val dataCategoryList = DataCategory()
                        dataCategoryList.icon = jsonObject.getString("icon")
                        dataCategoryList.nameCategory = jsonObject.getString("category_name")
                        list.add(dataCategoryList)
                    }
                    categoryList.postValue(list)

                } catch (e: Exception) {
                    Log.d("API Result Error", "Error: ${e.message}")
                    Toast.makeText(MainActivity(), e.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable?
            ) {
                TODO("Not yet implemented")
            }

        })
    }

    fun getDataCategory(): LiveData<ArrayList<DataCategory>> {
        return categoryList
    }

    fun setDataBanner() {
        val list = arrayListOf<DataBanner>()

        val client = AsyncHttpClient()
        val url = "https://gardien.tokodistributor.co.id/api-web/v2/utility/home/banner-web"

        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
                val result = String(responseBody!!)
                Log.d("API Result", result)

                try {
                    val responseObjects = JSONObject(result)
                    val data = responseObjects.getJSONArray("data")

                    for (i in 0 until data.length()) {
                        val jsonObject = data.getJSONObject(i)

                        val dataBannerList = DataBanner()
                        dataBannerList.imageBanner = jsonObject.getString("url_mobile")
                        list.addAll(listOf(dataBannerList))
                    }
                    bannerList.postValue(list)

                } catch (e: Exception) {
                    Log.d("API Result Error", "Error: ${e.message}")
                    Toast.makeText(MainActivity(), e.message, Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>?,
                responseBody: ByteArray?,
                error: Throwable?
            ) {
                TODO("Not yet implemented")
            }

        })
    }

    fun getDataBanner(): LiveData<List<DataBanner>> {
        return bannerList
    }
}