package com.adit.product.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class DataProduct (
    var photoProduct: String? = null,
    var nameProduct: String? = null,
    var price: String? = null
) : Parcelable