package com.adit.product.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class DataCategory (
    var icon: String? = null,
    var nameCategory: String? = null
) : Parcelable