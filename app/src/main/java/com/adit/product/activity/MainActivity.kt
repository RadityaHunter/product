package com.adit.product.activity

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.adit.product.R
import com.adit.product.adapter.CarouselAdapter
import com.adit.product.adapter.CategoryAdapter
import com.adit.product.adapter.ProductAdapter
import com.adit.product.databinding.ActivityMainBinding
import com.adit.product.fragment.CarouselItemFragment
import com.adit.product.mvvm.MainViewModel
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private lateinit var productAdapter: ProductAdapter
    private lateinit var categoryAdapter: CategoryAdapter

    private val mainViewModel: MainViewModel by viewModels()

    private lateinit var carouselItems: List<CarouselItemFragment>
    private lateinit var timer: Timer
    var currentCarouselIndex = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = resources.getString(R.string.title_name)

        showLoading(true)

        showListProduct()
        showDataProduct()
        showListCategory()
        showDataCategory()
        showDataCarousel()

        buildIndicator()
    }

    // PRODUCT
    private fun showDataProduct() {
        mainViewModel.setDataProduct()
        mainViewModel.getDataProduct().observe(this, { data ->
            if (data != null) {
                productAdapter.setData(data)
                showLoading(false)
            }
        })
    }

    private fun showListProduct() {
        binding.layoutProduct.rvProduct.setHasFixedSize(true)
        binding.layoutProduct.rvProduct.layoutManager = GridLayoutManager(this, 2)
        productAdapter = ProductAdapter()
        productAdapter.notifyDataSetChanged()
        binding.layoutProduct.rvProduct.adapter = productAdapter
    }

    //CATEGORY
    private fun showListCategory() {
        binding.layoutCategory.rvCategory.setHasFixedSize(true)
        binding.layoutCategory.rvCategory.layoutManager = LinearLayoutManager(this,GridLayoutManager.HORIZONTAL,false)
        categoryAdapter = CategoryAdapter()
        categoryAdapter.notifyDataSetChanged()
        binding.layoutCategory.rvCategory.adapter = categoryAdapter
    }

    private fun showDataCategory() {
        mainViewModel.setDataCategory()
        mainViewModel.getDataCategory().observe(this, { dataCategory ->
            if (dataCategory != null) {
                categoryAdapter.setData(dataCategory)
                showLoading(false)
            }
        })

    }

    //CAROUSEL START
    private fun showDataCarousel() {
        carouselItems = listOf(
            CarouselItemFragment.newInstance(resources.getString(R.string.banner_1)),
            CarouselItemFragment.newInstance(resources.getString(R.string.banner_2)),
            CarouselItemFragment.newInstance(resources.getString(R.string.banner_3)),
            CarouselItemFragment.newInstance(resources.getString(R.string.banner_4)),
            CarouselItemFragment.newInstance(resources.getString(R.string.banner_5))
        )
        val carouselAdapter = CarouselAdapter(carouselItems, this)

        val viewPager2: ViewPager2 = binding.layoutCarousel.viewPagerCarousel
        viewPager2.adapter = carouselAdapter

        viewPager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)

                var indicatorView =
                    binding.layoutCarousel.carouselIndicator.getChildAt(currentCarouselIndex) as ImageView
                indicatorView.setImageResource(R.drawable.default_selected_dot)

                indicatorView = if (currentCarouselIndex == 0) {
                    binding.layoutCarousel.carouselIndicator.getChildAt(carouselItems.size - 1) as ImageView
                } else {
                    binding.layoutCarousel.carouselIndicator.getChildAt(currentCarouselIndex - 1) as ImageView
                }
                indicatorView.setImageResource(R.drawable.default_unselected_dot)
            }
        })
    }


    private fun buildIndicator() {
        carouselItems.mapIndexed { index, _ ->
            val imageView = ImageView(this)
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.marginEnd = 10
            imageView.layoutParams = params
            if (index == 0) imageView.setImageResource(R.drawable.default_selected_dot)
            else imageView.setImageResource(R.drawable.default_unselected_dot)
            binding.layoutCarousel.carouselIndicator.addView(imageView)
        }
    }

    override fun onResume() {
        super.onResume()
        timer = Timer()
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                if (currentCarouselIndex == carouselItems.size - 1) {
                    currentCarouselIndex = 0
                } else {
                    currentCarouselIndex++
                }
                runOnUiThread {
                    binding.layoutCarousel.viewPagerCarousel.setCurrentItem(
                        currentCarouselIndex,
                        true
                    )
                }
            }
        }, 8000, 8000)
    }

    override fun onPause() {
        super.onPause()
        timer.cancel()
    }
    //CAROUSEL END

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun showLoading(progress: Boolean) {
        if (progress) {
            binding.layoutProduct.progressBar.visibility = View.VISIBLE
            binding.layoutCategory.progressBar2.visibility = View.VISIBLE
        } else {
            binding.layoutProduct.progressBar.visibility = View.GONE
            binding.layoutCategory.progressBar2.visibility = View.GONE
        }
    }
}