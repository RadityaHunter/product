package com.adit.product.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.adit.product.R
import com.adit.product.databinding.CarouselItemFragmentBinding
import com.adit.product.mvvm.MainViewModel
import com.bumptech.glide.Glide

class CarouselItemFragment : Fragment() {
    private lateinit var _binding: CarouselItemFragmentBinding
    private val binding get() = _binding

    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = CarouselItemFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val imageBanner = arguments?.getString(IMAGE, "")

        if (imageBanner != null) {
            viewModel.setDataBanner()
        }
        viewModel.getDataBanner().observe(viewLifecycleOwner, { dataBanner ->
            if (dataBanner != null) {
                Glide.with(requireActivity())
                    .load(imageBanner)
                    .placeholder(R.drawable.image_not_found)
                    .into(binding.imageCarousel)
            }
        })

        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        const val IMAGE = "IMAGE"

        fun newInstance(image: String): CarouselItemFragment {
            val fragment = CarouselItemFragment()
            val bundle = Bundle()
            bundle.putString(IMAGE, image)
            fragment.arguments = bundle
            return fragment
        }
    }
}
